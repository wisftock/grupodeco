const fetchGet = async () => {
  const token = localStorage.getItem('Authorization');
  const response = await fetch(
    `https://apievaluacion20230228071438.azurewebsites.net/api/ProductCatalog`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  const result = response.json();
  return result;
};

const fetchPost = async () => {
  const data = {
    Username: 'test20',
    Password: 'test20',
  };
  const response = await fetch(
    `https://apievaluacion20230228071438.azurewebsites.net/api/Authenticate`,
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }
  );

  const result = response.json();
  return result;
};

export { fetchGet, fetchPost };

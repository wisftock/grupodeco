import React from 'react';
import { fetchPost } from '../api/api';
const Navbar = () => {
  const generateToken = () => {
    fetchPost().then(({ token }) => {
      localStorage.setItem('Authorization', token);
    });
  };
  return (
    <>
      <nav className='navbar navbar-expand-lg '>
        <div className='container-fluid'>
          <a className='navbar-brand' href='#'>
            Botica
          </a>
          <button
            className='navbar-toggler'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#navbarSupportedContent'
            aria-controls='navbarSupportedContent'
            aria-expanded='false'
            aria-label='Toggle navigation'
          >
            <span className='navbar-toggler-icon'></span>
          </button>
          <div className='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul className='navbar-nav me-auto mb-2 mb-lg-0'></ul>
            <button className='btn btn-outline-success' onClick={generateToken}>
              token
            </button>
            <button className='btn btn-primary'>
              <i className='fa-solid fa-cart-shopping'></i>
            </button>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;

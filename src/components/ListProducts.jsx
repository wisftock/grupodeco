import React, { useEffect, useState } from 'react';
import { fetchGet } from '../api/api';

const ListProducts = () => {
  const [data, setData] = useState([]);
  const [listDataProduct, setListDataProduct] = useState([]);

  const listProduct = () => {
    fetchGet().then((lista) => {
      setData(lista);
    });
  };

  const handleAddProduct = (datos) => {
    const variable = [];
    variable.push(datos);
    localStorage.setItem('product', JSON.stringify(variable));
    setListDataProduct(variable);
  };

  useEffect(() => {
    listProduct();
  }, []);

  return (
    <div className='container mt-4 container_listProduct'>
      {data.map((datos, key) => {
        const { name, imag, code } = datos;
        return (
          <div className='card' key={key}>
            <div className='card_img'>
              <p className='card-title'>{name}</p>
              <img src={imag} className='card-img-top' alt='...' />
            </div>
            {/* <div className='card-body'>
              <span>{code}</span>
              <div>
                <button>
                  <i className='fa-sharp fa-solid fa-trash'></i>
                </button>
                1<button>+</button>
              </div>
            </div> */}
            <a
              href='#'
              className='btn btn-primary mt-4'
              onClick={() => handleAddProduct(datos)}
            >
              Agregar
            </a>
          </div>
        );
      })}

      <h2>Prueba</h2>
      <code>{localStorage.getItem('product')}</code>
    </div>
  );
};

export default ListProducts;

import React, { useState } from 'react';
import CardsProducts from './components/CardsProducts';

import ListProducts from './components/ListProducts';
import Navbar from './components/Navbar';

const App = () => {
  return (
    <div>
      <Navbar />
      <ListProducts />
      <CardsProducts />
    </div>
  );
};

export default App;
